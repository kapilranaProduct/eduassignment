import React, { useState } from 'react';
import './App.css';
import UserFilter from "user-filter-edu/dist/components/user_filter/UserFilter";

function App() {

  const [data, setData] = useState([]);
  const maxDistance = 100;
  const initialDistance = { lat: 53.339428, lng: -6.257664 }

  const changeFile = (event) => {
    const file = event.target.files[0];
    if (file) {
      const fileReder = new FileReader()
      fileReder.onload = function (event) {
        const allResult = event.currentTarget.result
        if (allResult) {
          const lines = allResult.split('\n')
          if (lines.length) {
            const parseLines = lines.map(item => JSON.parse(item))
            setData(parseLines);
          } else {
            alert('Invalid Data format')
          }
        }
      }

      fileReder.readAsText(file)
    }
  }

  return (
    <div className="App">
      <p>Please select customer txt file</p>
      <input accept="text/plain"  onChange={changeFile} type="file" />
      {data.length ? <UserFilter
        data={data}
        sortType="asc"
        maxDistance={maxDistance}
        initialDistance={initialDistance} /> : null}
    </div>
  );
}


/* UserFilter(npm made by me) component accept Props as following
1. data: isRequired(Array) = It is the data on which iteration occurs
2. sortType: optional(String 'asc' or 'dsc') = It defines the whether the output is sorted in ascending or descending.
    *Note by default their is no sort in the resultant output
3. initialDistance: isRequired(Object {lat, lng}) = It is the initial distance from which you want to calculate distance
4. maxDistance(In Km ): optional(number) = max distance which you want to filter
*/

export default App;
